def display_instruction():

    print('''
    Calculator
 ______________
| ____________||
||            ||
||            || 
||____________||
|              |
| |1||2||3||/| |
| |4||5||6||*| |
| |7||8||9||-| |
| |.||0||+||=| | 
|______________|

 __________________________________________
|                                          |
|              Instruction                 |
| Enter / if you want to devide numbers.   |
| Enter * if you want to multiply numbers. |
| Enter + if you want to add numbers.      |
| Enter - if you want to divide numbers.   |
|__________________________________________|
''')

def get_user_input(virable_type, prompt='integers'):
    return virable_type(input(f'Enter the {prompt}: '))

def display_error_message(date='integers'):
    print(f'Only {date} are allowed! Try again.')

def add(num1, num2):
    return num1 + num2

def subtract(num1, num2):
    return num1 - num2

def multiply(num1, num2):
    return num1 * num2

def divide(num1, num2):
    return round(num1 / num2, 2)

def get_selected_function(operator):
    return possible_operations[operator]

def calculate_result(chosen_function, val1, val2):
    return chosen_function(val1, val2)

def set_output(result):
    return ((12 - len(str(result))) * ' ' + str(result))
    
def display_result(result):

    print(f'''
    Result
 ______________
| ____________ |
||            ||
||{set_output(result)}|| 
||____________||
|              |
| |1||2||3||/| |
| |4||5||6||*| |
| |7||8||9||-| |
| |.||0||+||=| | 
|______________|
''')

possible_operations = {'+': add, '-': subtract, '/': divide, '*': multiply}
