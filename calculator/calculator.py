from utils import display_instruction, get_user_input, display_error_message
from utils import get_selected_function, calculate_result, display_result
from utils import possible_operations

display_instruction()

while True:
    try:
        num1 = get_user_input(int, 'first number')
        break
    except ValueError:
        display_error_message()

operator = get_user_input(str, 'operator')
while operator not in possible_operations.keys():
    display_error_message('+, -, /, *')
    operator = get_user_input(str, 'operator')

while True:
    try:
        num2 = get_user_input(int, 'second number')
        break
    except ValueError:
        display_error_message()


selected_function = get_selected_function(operator)

result = calculate_result(selected_function, num1, num2)

display_result(result)